angular.module('app',['ui.router', 'ui.grid']);


angular.module('app').config(function($stateProvider, $urlRouterProvider) {
  
    $urlRouterProvider.otherwise("home");
  
    var homeState = {
      name: 'home',
      url: '/home',
      templateUrl: 'src/app/homePage/homePage.html',
      controller: 'HomePageController',
      controllerAs: 'homeCtrl'
    };

    var aboutState = {
      name: 'about',
      url: '/about',
      template: '<h3>Its the UI-Router hello world app! - ABOUT PAGE</h3>'
    };

    var contactState = {
      name: 'contact',
      url: '/contact',
      template: '<h3>hello world! - CONTACT PAGE</h3>'
    };
    
    var uiGridState = {
      name: 'uiGridPage',
      url: '/uiGridPage',
      templateUrl: 'src/app/uiGridPage/uiGridPage.html',
      controller: 'UiGridPageController',
      controllerAs: 'uiGridCtrl'
    };
    
    var ngFormState = {
      name: 'ngFormPage',
      url: '/ngFormPage',
      templateUrl: 'src/app/ngFormPage/ngFormPage.html',
      controller: 'NgFormPageController',
      controllerAs: 'ngFormCtrl'
    };
    
    var modalsState = {
      name: 'modalsPage',
      url: '/modalsPage',
      templateUrl: 'src/app/modalsPage/modalsPage.html',
      controller: 'ModalsPageController',
      controllerAs: 'modalsCtrl'
    };

    $stateProvider.state(homeState);
    $stateProvider.state(aboutState);
    $stateProvider.state(contactState);
    $stateProvider.state(uiGridState);
    $stateProvider.state(ngFormState);
    $stateProvider.state(modalsState);
});