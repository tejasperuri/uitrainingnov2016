angular.module('app')
.controller('ModalsPageController', ['$scope','$rootScope','myServiceUtil','myFactoryUtil','jsonService', '$filter',
        function ($scope, $rootScope,  myServiceUtil, myFactoryUtil, jsonService, $filter) {
    var vm = this;
    vm.user = {};
    // function to submit the form after all validation has occurred			
    vm.submitForm = function() {
        // check to make sure the form is completely valid
        if ($scope.userForm.$valid) {
            alert('our form is amazing');
            console.log(vm.user);
        }
    };
    
}]);