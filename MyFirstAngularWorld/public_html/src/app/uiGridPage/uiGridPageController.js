angular.module('app')
.controller('UiGridPageController', ['$scope','$rootScope','myServiceUtil','myFactoryUtil','jsonService', '$filter',
        function ($scope, $rootScope,  myServiceUtil, myFactoryUtil, jsonService, $filter) {
    var vm = this;
    vm.gridOptions = {
        enableSorting: true,
        columnDefs: [
            {
                field: 'name',
                displayName: 'Full Name'
            },
            { field: 'gender' },
            { 
                field: 'company',
                enableSorting: false,
                displayName: 'Company'
            }
        ],
        onRegisterApi: function( gridApi ) {
            vm.grid1Api = gridApi;
        }
    };
    jsonService.uiGridData().then(function(response){
        vm.gridOptions.data = response;
        console.log(response);
    });
    
}]);