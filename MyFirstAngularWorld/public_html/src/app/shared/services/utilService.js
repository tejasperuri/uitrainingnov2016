angular.module('app').service('myServiceUtil', function(){
    this.sayHello = function(name) {
        return "Hi " + name +"!";
    };
    
    this.isEmpty = function(input){
//        if(input) {
//            return true;
//        } else {
//            return false;
//        }
        
        return input ? true : false;
    };
});

angular.module('app').factory('myFactoryUtil', function(){
    return {
        sayHello : function(name) {
            return "Hi " + name +"!";
        },
        isEmpty : function(input) {
            return input ? true : false;
        }
    };
    
});

