angular.module('app').factory('jsonService',  ['$rootScope','$http', '$q', '$log', 
    function($rootScope, $http, $q, $log){
        return {
            allUsers : function() {
                var deferred = $q.defer();
                $http.get("src/app/mock/data/allUserData.json").then(function (response){
                    //console.log(response);
                    deferred.resolve(response.data);
                }).catch(function (response){
                    console.log(response);
                    deferred.reject("http error");
                });
//                  Deprected
//                $http.get("src/app/mock/data/allUserData.json").success(function (response){
//                    //console.log(response);
//                    deferred.resolve(response.data);
//                }).error(function (response){
//                    //console.log(response);
//                    deferred.reject("http error");
//                });

                return deferred.promise;
            },
            postCallTesting:  function(postData) {
                var deferred = $q.defer();
                $http.post("src/app/mock/data/allUserData.json", postData).then(function (response){
                    //console.log(response);
                    deferred.resolve(response.data);
                }).catch(function (response){
                    console.log(response);
                    deferred.reject("http error");
                });
                return deferred.promise;
            },
            uiGridData : function() {
                var deferred = $q.defer();
                $http.get("src/app/mock/data/uiGridData.json").then(function (response){
                    //console.log(response);
                    deferred.resolve(response.data);
                }).catch(function (response){
                    console.log(response);
                    deferred.reject("http error");
                });
                return deferred.promise;
            }
        };
    
}]);