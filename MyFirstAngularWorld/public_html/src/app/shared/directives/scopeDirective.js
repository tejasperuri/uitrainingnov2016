angular.module('app').directive('scopeDirective', function(){
    return {
        restrict: "EA",
        scope:{
            myVal: "@"
        },
//        template: "<input type='text' ng-model='myVal'></input>",
        template: "<input type='submit' ng-click='myVal()'></input>",
        
    };
    
});