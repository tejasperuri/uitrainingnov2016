angular.module('app').directive('firstDirective', function(){
    return {
        restrict: "EA", // E- Element, A - Attribute, C - Class, M - comments
        template: "<div>First Name: Tejas</div><div>Last Name: Peruri</div>",
        link: function() {
            console.log("Hello from first directive");
        }
    };
    
});