(function(){
   function reverseText(){
        return function(input){
            input = input || "";

            var result = "";
            if(input) {
               //result = input.split('').reverse().join('');

               for(var i=0; i<input.length; i++) {
                   result = input.charAt(i) + result;
               }
            }


            return result;
        };
    };
    function capitalize(){
        return function(input){
            input = input || "";

            var result = "";
            if(input) {
               result = input.toUpperCase();
            }


            return result;
        };
    };
    angular.module('app').filter('reverseText',reverseText);
    angular.module('app').filter('capitalize', capitalize);
})();



//angular.module('app').filter('reverseText', function(){
//    return function(input){
//        input = input || "";
//        
//        var result = "";
//        if(input) {
//           //result = input.split('').reverse().join('');
//           
//           for(var i=0; i<input.length; i++) {
//               result = input.charAt(i) + result;
//           }
//        }
//        
//        
//        return result;
//    };
//    
//});
//
//angular.module('app').filter('capitalize', function(){
//    return function(input){
//        input = input || "";
//        
//        var result = "";
//        if(input) {
//           result = input.toUpperCase();
//        }
//        
//        
//        return result;
//    };
//    
//});

