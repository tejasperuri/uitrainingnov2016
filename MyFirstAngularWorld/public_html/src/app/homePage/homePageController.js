//angular.module('app').controller('HomePageController', function($scope, $rootScope,  myServiceUtil, myFactoryUtil){
//    $scope.test1="Hi its $scope variable - HomeController";
//    $rootScope.test2="Hi its $rootScope variable - HomeController";
//    var vm = this;
//    vm.pageName = "HOME PAGE";
//    
//    
//    console.log("Say Hello My Service" + myServiceUtil.sayHello("Nihar"));
//    console.log("Is Empty My Service" + myServiceUtil.isEmpty(""));
//    
//    console.log("Say Hello My Factory" + myFactoryUtil.sayHello("Pratik"));
//    console.log("Is Empty My Factory" + myFactoryUtil.isEmpty(""));
//});

angular.module('app')
.controller('HomePageController', ['$scope','$rootScope','myServiceUtil','myFactoryUtil','jsonService', '$filter',
        function ($scope, $rootScope,  myServiceUtil, myFactoryUtil, jsonService, $filter) {
  $scope.test1="Hi its $scope variable - HomeController";
    $rootScope.test2="Hi its $rootScope variable - HomeController";
    var vm = this;
    vm.pageName = "home Page";
    
   
    
    console.log("Say Hello My Service " + myServiceUtil.sayHello("Nihar"));
    console.log("Is Empty My Service " + myServiceUtil.isEmpty("test"));
    
    console.log("Say Hello My Factory " + myFactoryUtil.sayHello("Pratik"));
    console.log("Is Empty My Factory " + myFactoryUtil.isEmpty("test"));
    
    //console.log("ReverseME ::" + $filter('reverseText')("ReverseME"));
    
    var x = 20;
    var y = 20;
    
    if(x == y) {
        console.log("x == y");
    }
        
    if(x === y) {
        console.log("x === y");
    }
    
    vm.allUserDetails = {};
    
    jsonService.allUsers().then(function(response){
        vm.allUserDetails = response;
        console.log(vm.allUserDetails);
    });
//    var data = $.param({
//            json: JSON.stringify({
//                name: $scope.newName
//            })
//        });
//    jsonService.postCallTesting(data).then(function(response){
//        vm.postCallResponse = response;
//        console.log(vm.postCallResponse);
//    });
//    
    console.log(vm.allUserDetails);
    
    vm.moviesList = [
        {
           name: "Dangal",
           genre: "sports",
           year: "2016",
           rating: 5
        },
        {
            name: "Hidden Figures",
            genre: "True Story",
            year: "2016",
            rating: 3
        },
        {
            name: "X-Men",
            genre: "Action",
            year: "2017",
            rating: 4
        },
        {
            name: "Angrez",
            genre: "Comedy",
            year: "2006",
            rating: 5
        },
        {
            name: "Zootopia",
            genre: "Animated",
            year: "2016",
            rating: 3
        },
        {
            name: "Yeh Jawani Hai Diwani",
            genre: "Romance",
            year: "2016",
            rating: 4
        }
    ]
    
    vm.showPopup = function() {
        alert("Hi I am a popup/alert");
    };
}]);